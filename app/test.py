from nodes import User
import static

def out(feed):
    for block in feed:
        print "Q: {0} \n{5}\n{1:<20}({2})\n{3}\n{4} upvotes\n".format(block["question"],
                                                                  block["name"],
                                                                  block["time"],
                                                                  block["content"],
                                                                  block["upvotes"],
                                                                  "-" * 40)

        print "classes: {0}, {1}".format(block["class_upvote"], block["class_bookmark"])
        print "*"*50

def main():
    niki = User("niknaknel").find()
    liso = User("kliso").find()
    q = static.get_question_by_id("4158620")
    a = static.get_answer_by_id("5566986")

    ### works ###
    # print static.test_relationship(niki, q, "BOOKMARKED")
    # print static.test_relationship(niki, liso, "FOLLOWS")
    # print static.test_relationship(liso, niki, "FOLLOWS")
    # print static.test_relationship(niki, a, "LIKES")

    ### works ###
    niki = User("niknaknel")
    feed = niki.build_feed("upvotes")
    #feed = niki.build_profile_feed()
    out(feed)
    print niki.get_following()


if __name__ == "__main__":
    main()
