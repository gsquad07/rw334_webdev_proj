from py2neo import authenticate, Graph, Node, Relationship

def connect():
    # connect to DB
    ip = "35.233.101.164:7473"
    uri = "https://" + ip + "/db/data/"
    authenticate(ip, "neo4j", "gsquad")
    graph = Graph(uri, bolt=False)
    print("connected!")
    return graph
