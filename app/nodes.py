from py2neo import Graph, Node, Relationship
from static import build_block, get_question, build_user_block, search_people
from datetime import datetime
from passlib.hash import bcrypt
from connect import connect
import uuid

graph = connect()
distinct = lambda x : [dict(t) for t in set([tuple(d.items()) for d in x])]

class User:
    def __init__(self, username):
        self.username = username

    # Reference: Nicole White Neo4j-Flask Tutorial
    def find(self):
        user = graph.find_one('User', 'username', self.username)
        return user

    #1.Register for first time users.
    def register(self, password):
        if (not self.find()):
            user = Node('User', username=self.username, password = bcrypt.encrypt(password), bio="", photo="", name="")
            graph.create(user)
            return True
        else:
            return False
    #2.Login with username and password.
    def verify_password(self, password):
        user = self.find()
        if (user):
            return bcrypt.verify(password, user['password'])
            #return password == user['password']
        else:
            return False

    #3.Profile page, can add a photo, write a short bio, change password (add a display_name).
    def add_display_name(self, name):
        user = self.find()
        user['name']= name
        graph.push(user)
        return

    def add_photo(self, photo):
        user = self.find()
        user['photo'] = photo
        graph.push(user)
        return

    def add_bio(self, bio):
        user = self.find()
        user['bio'] = bio
        graph.push(user)
        return

    def change_password(self, new_password):
        user = self.find()
        user['password'] = bcrypt.encrypt(new_password)
        graph.push(user)
        return

    #4.Be able to follow other people registered by searching via name:
    def follow(self, u2):
        user1 = self.find()
        user2 = graph.find_one('User', 'username', u2)
        rel = Relationship(user1, "FOLLOWS", user2)
        graph.create(rel)
        return

    #6.Be able to select particular topics you want to follow from a list of predefined topics.
    def interested_in_topic(self, title): #Gets passed a Topic string *one topic at a time.
        topic = Node('Topic', title=title)
        graph.merge(topic)
        user = self.find()
        rel = Relationship(user, 'INTERESTED', topic)
        graph.create(rel)
        return

    #Looked at Nicoles method for add_post: uuid4()->Generates a random UUID
    #7. Be able to ask and answer questions
    def ask_question(self, text, topics):
        user = self.find()
        d = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        question = Node('Question', id=str(uuid.uuid4()), text=text, datetime=d)
        rel = Relationship(user, 'ASKED', question)
        graph.create(rel)

        for title in topics:
            tag = Node('Topic', title= title)
            graph.merge(tag)
            #When asking a question, it must be tagged by one or more topics from a list of predefined topics.
            rel2 = Relationship(question, 'TAGGED', tag)
            graph.create(rel2)

        return

    def answer_question(self, content, question_id):
        user = self.find()
        question = graph.find_one('Question', 'id', question_id)
        d = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        answer = Node('Answer', id=str(uuid.uuid4()), content=content, datetime=d)
        graph.merge(answer)

        rel = Relationship(user, 'POSTED', answer)
        graph.create(rel)
        rel2 = Relationship(answer, 'ANSWERED', question)
        graph.create(rel2)
        return

    #8. Be able to upvote answers:
    def like(self, answer_id):
        user = self.find()
        answer = graph.find_one('Answer', 'id', answer_id)
        rel = Relationship(user, 'LIKES', answer)
        graph.merge(rel)
        return

    #12. Be able to bookmark questions with corresponding answers, for later view.
    def bookmark(self, question_id):
        user = self.find()
        question = graph.find_one('Question', 'id', question_id)
        rel = Relationship(user, 'BOOKMARKED', question)
        graph.create(rel)
        return

    # populate feed with questions asked by people/topics you follow
    def suggested_questions(self):
        query = """MATCH (u:User)-[:FOLLOWS]->(m)-[:ASKED]->(q:Question)
                    WHERE u.username  = \"{0}\"
                    RETURN q.id AS id,
                           q.text AS text,
                           q.datetime AS datetime
                    LIMIT 10""".format(self.username)

        q = graph.run(query).data()

        query = """MATCH (u:User)-[:INTERESTED]->(t)<-[:TAGGED]-(q:Question)
                        WHERE u.username  = \"{0}\"
                        RETURN q.id AS id,
                                q.text AS text,
                                q.datetime AS datetime
                        LIMIT 10""".format(self.username)

        q += graph.run(query).data()
        return q

    # populate feed with questions answered by people you follow
    def suggested_answers(self):
        query = """MATCH (u:User)-[:FOLLOWS]->(m)-[:POSTED]->(a)-[:ANSWERED]->(q:Question)
                        WHERE u.username  = \"{0}\"
                        RETURN q.id AS id,
                               q.text AS text,
                               q.datetime AS datetime
                        LIMIT 10""".format(self.username)
        q = graph.run(query).data()
        return q

    def my_questions(self):
        query = """MATCH (u:User)-[:ASKED]->(q:Question)
                                WHERE u.username  = \"{0}\"
                                RETURN q.id AS id,
                                       q.text AS text,
                                       q.datetime AS datetime
                                LIMIT 10""".format(self.username)
        q = graph.run(query).data()
        return q

    def my_answers(self):
        query = """MATCH (u:User)-[:POSTED]->(a:Answer)
                                WHERE u.username  = \"{0}\"
                                RETURN a.id AS id,
                                       a.content AS content,
                                       a.datetime AS datetime
                                LIMIT 10""".format(self.username)
        q = graph.run(query).data()
        return q


    def get_followers(self, count=False):
        if count:
            query = """MATCH (u:User)<-[:FOLLOWS]-(f:User)
                                WHERE u.username  = \"{0}\"
                                RETURN count(f) AS count""".format(self.username)
            q = graph.run(query).data()[0]["count"]

        else:
            query = """MATCH (u:User)<-[:FOLLOWS]-(f:User)
                                WHERE u.username  = \"{0}\"
                                RETURN f.username AS username,
                                       f.name AS name,
                                       f.bio AS bio,
                                       f.photo AS photo""".format(self.username)
            q = graph.run(query).data()
        return q


    def get_following(self, count=False):
        if count:
            query = """MATCH (u:User)-[:FOLLOWS]->(f:User)
                                WHERE u.username  = \"{0}\"
                                RETURN count(f) AS count""".format(self.username)
            q = graph.run(query).data()[0]["count"]

        else:
            query = """MATCH (u:User)-[:FOLLOWS]->(f:User)
                                WHERE u.username  = \"{0}\"
                                RETURN f.username AS username,
                                       f.name AS name,
                                       f.bio AS bio,
                                       f.photo AS photo""".format(self.username)
            q = graph.run(query).data()
        return q


    def get_follow_suggestions(self):
        q = """MATCH (u:User)-[:FOLLOWS]->(m)-[:FOLLOWS]->(influencer:User),
			        (liker)-[:LIKES]->(a:Answer)<-[:POSTED]-(influencer)
			        WHERE NOT (u)-[:FOLLOWS]->(influencer) AND u <> influencer
			        AND u.username = \"{0}\"
			        RETURN influencer.username AS username, count(liker) AS Strength
			        ORDER BY Strength DESC""".format(self.username)
        users = graph.run(q).data()
        return users

    def get_bookmarks(self):
        q = """MATCH (u:User)-[:BOOKMARKED]->(q:Question)
               WHERE u.username = \"{0}\"
               RETURN q.id AS id, q.text AS text, q.datetime AS datetime
            """.format(self.username)

        return graph.run(q).data()

    def build_feed(self, key):
        data = distinct(self.suggested_answers() + self.suggested_questions())
        feed = []

        for q in data:
            feed.append(build_block(self, q))

        feed = filter(None, feed)
        if key == "time":
            feed = sorted(feed, key=lambda k: str(k[key]), reverse=True)
        else:
            feed = sorted(feed, key=lambda k: int(k[key]), reverse=True)
        return feed


    def build_profile_feed(self):
        my_quest = self.my_questions()
        my_ans = self.my_answers()
        corr_quest = [get_question(a) for a in my_ans]
        feed = []

        for q in my_quest:
            feed.append(build_block(self, q))

        for a, q in zip(my_ans, corr_quest):
            feed.append(build_block(self, q, a=a))

        feed = filter(None, feed)
        feed = sorted(feed, key=lambda k: str(k["time"]), reverse=True)
        return feed


    def build_suggestions_feed(self):
        unames = self.get_follow_suggestions()
        users = [ User(u["username"]) for u in unames]
        feed = []

        for u in users:
            # this can be used with a jinja %for% in the html
            feed.append(build_user_block(self, u.find()))

        return feed


    def build_search_feed(self, search_key):
        unames = search_people(self.username, search_key)
        users = [User(u["username"]) for u in unames]
        feed = []

        for u in users:
            feed.append(build_user_block(self, u))

        return feed
