import os
from flask import Flask, render_template, redirect, url_for, session, request, flash
from py2neo import Graph, Node
#from werkzeug import secure_filename
from connect import connect
from nodes import User
import traceback
from static import get_question_by_id, build_answer_feed, test_relationship, build_search_feed
#username = "kliso"
graph = connect()

UPLOAD_FOLDER = '/profile_images'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.secret_key = os.urandom(24)

@app.route('/css/<file>') # the route name, <file> is like a request.args
def css():
  return render_template(file)

@app.route('/')
def index():
    return render_template('index.html')

######################################################################################################################

# Login/register

@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        display = request.form['display']

        if(len(username) < 1 or len(password) < 1 or len(display) < 1):
            error = 'Please fill in all fields.'
            return render_template('index.html', error=error)

        if(User(username).register(password)):
            User(username).add_display_name(display)
            #topics
            if (request.form.get('food')):
                food = request.form['food']
                User(username).interested_in_topic(food)
            if (request.form.get('movies')):
                movies = request.form['movies']
                User(username).interested_in_topic(movies)
            if (request.form.get('fashion')):
                fashion = request.form['fashion']
                User(username).interested_in_topic(fashion)
            if (request.form.get('books')):
                books = request.form['books']
                User(username).interested_in_topic(books)
            if (request.form.get('diy')):
                diy = request.form['diy']
                User(username).interested_in_topic(diy)
            if (request.form.get('science')):
                science = request.form['science']
                User(username).interested_in_topic(science)

            session['username'] = username
            return redirect(url_for('home'))
        else:
            error = 'A user with that username already exists.'
            return render_template('index.html', error=error)

    return render_template('index.html')

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if(User(username).verify_password(password)):
            session['username'] = username
            return redirect(url_for('home'))
        else:
            error = 'Invalid login'
            return render_template('index.html', error=error)
    return render_template('index.html')

@app.route('/logout')
def logout():
    session.pop('username', None)
    msg = 'You have logged out.'
    return render_template('index.html', msg=msg)

######################################################################################################################

# Profile tings

@app.route('/profile/<username>')
def profile(username):
    logged_in_username = session.get('username')
    user_being_viewed_username = username

    if (logged_in_username == username):
        user = User(logged_in_username)

        count = user.get_followers(True)
        followers = user.get_followers()

        follow_count = user.get_following(True)
        following = user.get_following()

        feed = user.build_profile_feed()

        bookmarks = user.get_bookmarks()

        return render_template('profile_home.html', user=user.find(), count=count, followers=followers, follow_count=follow_count, following=following, feed=feed, bookmarks=bookmarks)

    else:
        user = User(user_being_viewed_username)

        count = user.get_followers(True)
        followers = user.get_followers()

        follow_count = user.get_following(True)
        following = user.get_following()

        feed = user.build_profile_feed()

        return render_template('profile_home.html', user=user.find(), count=count, followers=followers, follow_count=follow_count, following=following, feed=feed)


#Profile Settings:
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/profile_settings/', methods=['POST', 'GET'])
def profile_settings():
    if request.method == 'POST':
        username = session.get('username')
        if request.form['button'] == 'Upload Bio':
            bio = request.form['bio_input']
            User(username).add_bio(bio)
            bio_msg = 'Bio updated!'
            return render_template('profile_settings.html', bio_msg=bio_msg)

        if request.form['button'] == 'Confirm new password':
            pw = request.form['pw']
            User(username).change_password(pw)
            pw_msg = 'Password updated!'
            return render_template('profile_settings.html', pw_msg=pw_msg)
    return render_template("profile_settings.html")

@app.route('/upload_file', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
            f = request.files['file']
            username = session.get('username')
            new_name = username + '_profile_pic'
            if allowed_file(f.filename):
                f.save('static/profile_images/' + username + '_profile_pic')
                User(username).add_photo(new_name)
                pic_msg = 'File uploaded successfully!'
            return render_template('profile_settings.html', pic_msg=pic_msg, new_name=new_name)

#######################################################################################################################

# Questions & Answers

@app.route('/post_question', methods=['POST'])
def post_question():
    text = request.form['text']
    topics = []
    username = session.get('username')
    user = User(username).find()
    #topics
    if (request.form.get('food')):
        topics.append(request.form.get('food'))
    if (request.form.get('movies')):
        topics.append(request.form.get('movies'))
    if (request.form.get('fashion')):
        topics.append(request.form.get('fashion'))
    if (request.form.get('books')):
        topics.append(request.form.get('books'))
    if (request.form.get('diy')):
        topics.append(request.form.get('diy'))
    if (request.form.get('science')):
        topics.append(request.form.get('science'))

    if(len(topics) == 0):
        error = "You must tag your question with atleast one topic."
        return render_template('home.html', error=error, user=user)

    elif(not text):
        error = "Your question needs a text body."
        return render_template('home.html', error=error, user=user)

    else:
        User(username).ask_question(text, topics)
        msg = "Question has been posted."
        return render_template('home.html', msg=msg , user=user)


@app.route('/post_answer/?<q_id>&<loc>', methods=['GET','POST'])
def post_answer(q_id, loc):
    username = session.get("username")
    user = User(username)
    answer = request.form.get('answer')
    user.answer_question(answer, q_id)

    if loc == "profile_home":
        return profile(username)
    elif loc == "question":
        return question_answers(q_id)
    else:
        return home()

@app.route('/question_answers/<q_id>', methods=['GET', 'POST'])
def question_answers(q_id):
    username = session.get('username')
    user = User(username).find()
    q = get_question_by_id(q_id)

    if test_relationship(user, q, "BOOKMARKED"):
        class_bookmark = "btn bookmarked\" disabled"
    else:
        class_bookmark = "btn bookmark"

    ans = build_answer_feed(user, q)
    return render_template("question_answers.html", q=q, class_bookmark=class_bookmark, answers=ans, user=user)

#######################################################################################################################

# Main home functions

@app.route('/home/', methods=['GET','POST'])
def home():
    key = request.args.get("key")
    if key is None:
        key = "time"

    try:
        username = session.get("username")
        user = User(username)
        suggest = user.build_suggestions_feed()
        following_count = user.get_following(True)
        follower_count = user.get_followers(True)
        feed = user.build_feed(key)

        return render_template("home.html", feed=feed, key=key, suggest=suggest, follower_count=follower_count,
                               following_count=following_count, user=user.find())

    except Exception as e:
        tb = traceback.format_exc()
        return str(tb)

@app.route('/vote/?<q_id>&<a_id>&<loc>', methods=['GET','POST'])
def vote(q_id, a_id, loc):
    username = session.get("username")
    User(username).like(a_id)

    if loc == "profile_home":
        return profile(username)
    elif loc == "question":
        return question_answers(q_id)
    else:
        return home()

@app.route('/bookmark/?<q_id>&<loc>', methods=['GET','POST'])
def bookmark(q_id, loc):
    username = session.get("username")
    User(username).bookmark(q_id)

    if loc == "profile_home":
        return profile(username)
    elif loc == "question":
        return question_answers(q_id)
    else:
        return home()

#####################################################################################################################

# Follow user

@app.route('/search', methods=['POST'])
def search():
    username = session.get('username')
    key = request.form['search']

    results = build_search_feed(User(username), key)

    if not results:
        error = "No results found"
        return render_template('results.html', error=error)
    else:
        return render_template('results.html', results=results)

@app.route('/follow/<u2>', methods=['POST'])
def follow_user(u2):
    username = session.get('username')
    User(username).follow(u2)
    msg = "You are now following"+" " + u2

    return render_template('results.html', msg=msg)

#######################################################################################################################

if __name__=='__main__':
    app.debug = True
    app.run()