from connect import connect
graph = connect()

# check if relationships exist
def test_relationship(n1, n2, rel):
    if rel == "FOLLOWS":
        q = """MATCH (u1:User)-[r]->(u2:User)
               WHERE u1.username = \"{0}\" AND u2.username = \"{1}\"
               RETURN type(r) AS rel LIMIT 1""".format(n1["username"], n2["username"])
        result = graph.run(q).data()
        if len(result) > 0:
            return "FOLLOWS" == result[0]["rel"]
        else:
            return False

    if rel == "BOOKMARKED":
        q = """MATCH (u:User)-[r]->(q:Question)
                       WHERE u.username = \"{0}\" AND q.id = \"{1}\"
                       RETURN type(r) AS rel LIMIT 1""".format(n1["username"], n2["id"])
        result = graph.run(q).data()
        if len(result) > 0:
            return "BOOKMARKED" == result[0]["rel"]
        else:
            return False

    if rel == "LIKES":
        q = """MATCH (u:User)-[r]->(a:Answer)
                       WHERE u.username = \"{0}\" AND a.id = \"{1}\"
                       RETURN type(r) AS rel LIMIT 1""".format(n1["username"], n2["id"])
        result = graph.run(q).data()
        if len(result) > 0:
            return "LIKES" == result[0]["rel"]
        else:
            return False


# search for people
def search_people(user, key):
    # q = """MATCH (u:User)-[:FOLLOWS]->(f:User)
    #        WHERE u.username = \"{0}\" AND (f.name =~ '(?i){1}.*' OR f.username =~'(?i){1}.*')
    #        RETURN f.username AS username, f.name AS name, f.bio AS bio, f.photo AS photo
    #        ORDER BY name""".format(user, key)
    #
    # result = graph.run(q).data()

    q = """MATCH (f:User)
           WHERE f.name =~ '(?i){0}.*' OR f.username =~'(?i){0}.*'
           RETURN f.username AS username, f.name AS name, f.bio AS bio, f.photo AS photo
           ORDER BY name""".format(key)

    result = graph.run(q).data()
    return result

def build_search_feed(user, key):
    res = search_people(user.username, key)
    feed = []

    for r in res:
        feed.append(build_user_block(user, r))

    return feed

# get question by its id
def get_question_by_id(id):
    q = """MATCH (q:Question)
                    WHERE q.id = \"{0}\"
                    RETURN q.id AS id,
                           q.text AS text,
                           q.datetime AS datetime""".format(id)
    return graph.run(q).data()[0]

# get answer by its id
def get_answer_by_id(id):
    q = """MATCH (a:Answer)
                    WHERE a.id = \"{0}\"
                    RETURN a.id AS id,
                           a.content AS content,
                           a.datetime AS datetime""".format(id)
    return graph.run(q).data()[0]

# get question related to given answer
def get_question(a):
    q = """MATCH (a:Answer)-[:ANSWERED]->(q:Question)
                    WHERE a.id = \"{0}\"
                    RETURN q.id AS id,
                           q.text AS text,
                           q.datetime AS datetime""".format(a["id"])
    return graph.run(q).data()[0]

# get answer to given question (limited count)
def get_answers(q, limit):
    q = """MATCH (a:Answer)-[:ANSWERED]->(q:Question)
                WHERE q.id = \"{0}\"
                OPTIONAL MATCH (likes)-[:LIKES]->(a)-[:ANSWERED]->(q)
                RETURN a.id AS id,
                       a.content AS content,
                       a.datetime AS datetime,
                count(likes) AS Strength ORDER BY Strength DESC
                LIMIT {1}""".format(q["id"], limit)
    return graph.run(q).data()

# get user who posted given answer (limited count)
def get_user(a):
    q = """MATCH (u:User)-[:POSTED]->(a:Answer)
                    WHERE a.id = \"{0}\"
                    RETURN u.username AS username,
                           u.name AS name,
                           u.bio AS bio,
                           u.photo AS photo""".format(a["id"])
    return graph.run(q).data()[0]

# get number of likes for given answer (limited count)
def num_likes(a):
    q = """MATCH (u:User)-[:LIKES]->(a:Answer)
                    WHERE a.id = \"{0}\"
                    RETURN count(u) AS likes""".format(a["id"])
    return graph.run(q).data()[0]["likes"]

# build inline user block
def build_user_block(session_u, user):
    session_user = session_u.find()
    uname = user["username"]
    name = user["name"]
    bio = user["bio"]
    photo = user["photo"]
    followers = ""

    if test_relationship(session_user, user, "FOLLOWS"):
        class_follow = "btn following\" disabled"
    else:
        class_follow = "btn search"

    print(class_follow)
    block = { "username" : uname, #hidden input for creating follow relationship
              "name" : name,
              "bio" : bio,
              "photo" : photo,
              "followers" : followers,
              "class_follow": class_follow
              }

    return block

def build_answer_feed(session_user, q):
    # user who posted answer
    ans = get_answers(q, 10)
    feed = []

    for a in ans:
        feed.append(build_answer_block(session_user, a))

    return feed


def build_answer_block(session_user, ans):
    user = get_user(ans)
    thumb = user["photo"]
    name = user["name"]

    # id
    a_id = ans["id"]

    # time
    time = ans["datetime"]

    # content
    content = ans["content"]

    # num likes
    upvotes = num_likes(ans)
    if test_relationship(session_user, ans, "LIKES"):
        class_upvote = "btn voted\" disabled"
    else:
        class_upvote = "btn vote"

    # create block
    feed_block = {"a_id": a_id,
                  "content": content,
                  "time": time,
                  "thumbnail": thumb,
                  "name": name,
                  "upvotes": upvotes,
                  "class_upvote": class_upvote
                  }

    return feed_block

# build feed block
def build_block(session_u, q, a=None):
    # session user (buttons enabled/disabled)
    session_user = session_u.find()

    # q text
    text = q["text"]
    q_id = q["id"]
    if test_relationship(session_user, q, "BOOKMARKED"):
        class_bookmark = "btn bookmarked\" disabled"
    else:
        class_bookmark = "btn bookmark"

    # top answer or given answer
    if (a):
        # if answer given, don't fetch from DB
        ans = a
    else:
        # answer not given -> fetch top from DB
        ans = get_answers(q, 10)
        if ans:
            ans = ans[0]
        else:
            # if no answers found, populate block with empty strings
            feed_block = {"question": text,
                          "q_id": q_id,
                          "thumbnail": "",
                          "name": "",
                          "time": q["datetime"],
                          "content": "",
                          "upvotes": "100000",
                          "class_upvote" : "btn done",
                          "class_bookmark" : class_bookmark
                          }
            return feed_block

    # user who posted answer
    user = get_user(ans)
    thumb = user["photo"]
    name = user["name"]

    #id
    a_id = ans["id"]

    # time
    time = ans["datetime"]

    # content
    content = ans["content"]

    # num likes
    upvotes = num_likes(ans)
    if test_relationship(session_user, ans, "LIKES"):
        class_upvote = "btn voted\" disabled"
    else:
        class_upvote = "btn vote"

    # create block
    feed_block = {"question" : text,
                  "q_id" : q_id,
                  "a_id" : a_id,
                  "thumbnail": thumb,
                  "name": name,
                  "time": time,
                  "content": content,
                  "upvotes": upvotes,
                  "class_upvote" : class_upvote,
                  "class_bookmark" : class_bookmark
                  }

    return feed_block
